package io.pivotal.pipelinepracticum.flightstatus

import java.time.LocalDate

interface FlightStatusLookup {
    fun updatesFor(date: LocalDate): List<FlightUpdate>
}
