package io.pivotal.pipelinepracticum.flightsubscriptions.db

import javax.persistence.*

@Entity
@Table(name = "passenger")
data class PassengerJpaEntity(

        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,

        val name: String,

        @OneToMany(mappedBy = "passenger", fetch = FetchType.EAGER, cascade = [CascadeType.ALL], orphanRemoval = true)
        var subscriptions: List<FlightSubscriptionJpaEntity>

)
