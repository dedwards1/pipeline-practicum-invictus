package io.pivotal.pipelinepracticum.flightsubscriptions.web

import io.pivotal.pipelinepracticum.flightsubscriptions.*
import org.assertj.core.api.Assertions.assertThat
import org.fluentlenium.adapter.junit.FluentTest
import org.fluentlenium.core.action.Fill
import org.fluentlenium.core.filter.FilterConstructor.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Profile
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import java.lang.RuntimeException
import java.time.LocalDate

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [FlightSubscriptionsWebTestConfig::class], webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("FlightSubscriptionsWebTest")
class FlightSubscriptionsWebTest : FluentTest() {

    @LocalServerPort
    private lateinit var port: String

    @Before
    fun setUp() {
        goTo("http://localhost:${port}")
    }

    @Test
    fun `signing up and subscribing to flight updates`() {
        createPassenger("Chaalix")

        subscribeToFlight("Chaalix", "KS 244", "2018-11-05")
        subscribeToFlight("Chaalix", "KS 244", "2018-11-12")

        viewFlightsFor("Chaalix")

        listOfFlightsShouldInclude("KS 244, 2018-11-05: Delayed")
        listOfFlightsShouldInclude("KS 244, 2018-11-12: On Time")
    }

    private fun listOfFlightsShouldInclude(flightInfo: String) {
        assertThat(`$`("li").map { it.text() }).contains(flightInfo)
    }

    private fun viewFlightsFor(passengerName: String) {
        clickLink("View Flight Updates")
        clickLink(passengerName)
    }

    private fun subscribeToFlight(passengerName: String, flightNumber: String, date: String) {
        clickLink("Subscribe to Flight")
        chooseOptionFor("Passenger Name", passengerName)
        fillInputLabeled("Flight Number").with(flightNumber)
        fillInputLabeled("Date").with(date)
        submitForm()
    }

    private fun createPassenger(name: String) {
        clickLink("Sign up")
        fillInputLabeled("Name").with(name)
        submitForm()
    }

    private fun submitForm() {
        `$`("button", withText("Submit")).submit()
    }

    private fun clickLink(linkText: String) {
        `$`("a", withText(linkText)).click()
    }

    private fun fillInputLabeled(labelText: String): Fill<*> {
        val matchingLabels = `$`("label", withText(labelText))
        if(matchingLabels.size == 0) throw RuntimeException("Couldn't find a label with text ${labelText}")

        val label = matchingLabels[0]

        val inputId = label.attribute("for")
        val matchingInputs = `$`("input", withId(inputId))
        if(matchingInputs.size == 0) throw RuntimeException("Couldn't find an input associated with label ${labelText}. The label's for attribute referenced id '${inputId}'")
        val input = matchingInputs[0]

        return input.fill()
    }

    private fun chooseOptionFor(labelText: String, optionText: String) {
        val matchingLabels = `$`("label", withText(labelText))
        if(matchingLabels.size == 0) throw RuntimeException("Couldn't find a label with text ${labelText}")

        val label = matchingLabels[0]

        val selectId = label.attribute("for")
        val matchingSelects = `$`("select", withId(selectId))
        if(matchingSelects.size == 0) throw RuntimeException("Couldn't find a select associated with label ${labelText}. The label's for attribute referenced id '${selectId}'")
        val select = matchingSelects[0]

        val matchingOptions = select.find("option", withText(optionText))
        if(matchingOptions.size == 0) throw RuntimeException("Couldn't find an option with text '${optionText}' in the select element labeled '${labelText}'.")

        matchingOptions.click()
    }
}

@SpringBootApplication
@Profile("FlightSubscriptionsWebTest")
class FlightSubscriptionsWebTestConfig {
    @Bean
    fun passengerRepository() = FakePassengerRepository()

    @Bean
    fun subscribeToFlight(passengerRepository: PassengerRepository) = SubscribeToFlight(passengerRepository)

    @Bean
    fun subscribedFlightStatuses(): SubscribedFlightStatuses = object : SubscribedFlightStatuses {
        override fun forPassenger(passenger: Passenger): Map<Flight, String> = mapOf(
                Flight("KS 244", LocalDate.of(2018, 11, 5)) to "Delayed",
                Flight("KS 244", LocalDate.of(2018, 11, 12)) to "On Time"
        )
    }
}