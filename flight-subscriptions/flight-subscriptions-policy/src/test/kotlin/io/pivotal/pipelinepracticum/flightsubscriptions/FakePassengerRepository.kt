package io.pivotal.pipelinepracticum.flightsubscriptions

import java.util.*

class FakePassengerRepository : PassengerRepository {
    val passengers: MutableMap<String, Passenger> = mutableMapOf()

    override fun findPassengerById(id: String): Passenger? {
        return passengers[id]
    }

    override fun savePassenger(passenger: Passenger): Passenger {
        val id = passenger.id ?: UUID.randomUUID().toString()
        val savedPassenger = passenger.copy(id = id)
        passengers[id] = savedPassenger
        return savedPassenger
    }

    override fun findAllPassengers(): List<Passenger> {
        return passengers.values.toList()
    }
}
