package io.pivotal.pipelinepracticum.flightsubscriptions

data class Passenger(
        val name: String,
        val id: String? = null,
        val subscriptions: List<Flight> = emptyList()
)