package io.pivotal.pipelinepracticum.flightsubscriptions

interface PassengerRepository {
    fun savePassenger(passenger: Passenger): Passenger
    fun findPassengerById(id: String): Passenger?
    fun findAllPassengers(): List<Passenger>
}
